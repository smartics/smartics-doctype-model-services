# Service Management Model


## Overview

This project specifies a model to create a free [doctype add-on](https://www.smartics.eu/confluence/x/nQHJAw)
for the [projectdoc Toolbox](https://www.smartics.eu/confluence/x/1YEp)
for [Confluence](https://www.atlassian.com/software/confluence).

The add-on provides blueprints to create pages for

  * Service, Service Type
  * System, System Type
  * Configuration Item, Configuration Item Type
  * and [many more](https://www.smartics.eu/confluence/x/B4O5)!

It also provides a space blueprint to get started with service management
documentation quickly.

## Fork me!
Feel free to fork this project to adjust the model according to your project
requirements.

The Service Management Model and it's derived artifacts are licensed under
[Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/x/B4O5)
  * [projectdoc Toolbox Online Manual](https://www.smartics.eu/confluence/x/EAFk)
  * [Doctype Maven Plugin](https://www.smartics.eu/confluence/x/zgDqAw)
