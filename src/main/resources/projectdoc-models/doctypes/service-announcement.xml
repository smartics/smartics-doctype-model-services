<?xml version='1.0'?>
<!--

    Copyright 2017-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
    xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    id="service-announcement"
    base-template="standard"
    provide-type="standard-type"
    category="operations">
  <resource-bundle>
    <l10n>
      <name>Service Announcement</name>
      <description>
        Publish an announcement regarding a service.
      </description>
      <about>
        Announcements communicate with stakeholders information about a service.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Servicemitteilungen">Servicemitteilung</name>
      <description>
        Publizieren Sie Informationen zu einem Service.
      </description>
      <about>
        Mitteilungen kommunizieren Informationen zu einem Service an
        Stakeholder.
      </about>
      <type plural="Typen von Servicemitteilungen">Typ einer Servicemitteilung</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">service-announcement-type</param>
          <param
              name="property"
              key="projectdoc.doctype.common.type"/>
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.service-announcement.it-services">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">it-service</param>
          <param
              name="property"
              key="projectdoc.doctype.service-announcement.it-services"/>
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>IT Services</name>
        </l10n>
        <l10n locale="de">
          <name>IT-Services</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.service-announcement.date">
      <value><xml><![CDATA[<at:var at:name="day-date-picker" at:rawxhtml="true"/>]]></xml></value>
      <resource-bundle>
        <l10n>
          <name>Release Date</name>
        </l10n>
        <l10n locale="de">
          <name>Veröffentlichungsdatum</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.duration.from">
      <value><xml><![CDATA[<at:var at:name="day-date-picker" at:rawxhtml="true"/>]]></xml></value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <description>
            Set the start date of a period this announcement is referring to.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Spezifizieren Sie das Startdatum der Periode, die diese Mitteilung
            betrifft.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.duration.to">
      <value><xml><![CDATA[<at:var at:name="day-date-picker" at:rawxhtml="true"/>]]></xml></value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <description>
            Set the end date of a period this announcement is referring to.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Spezifizieren Sie das Endedatum der Periode, die diese Mitteilung
            betrifft.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.duration">
      <value>
        <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-transclusion-property-display">
                    <ac:parameter ac:name="property-name"><at:i18n at:key="projectdoc.doctype.common.duration.from"/></ac:parameter>
                    <ac:parameter ac:name="append-text"><at:i18n at:key="projectdoc.doctype.common.duration.delimiter"/></ac:parameter>
                    <ac:parameter ac:name="append">true</ac:parameter></ac:structured-macro><ac:structured-macro ac:name="projectdoc-transclusion-property-display">
                    <ac:parameter ac:name="property-name"><at:i18n at:key="projectdoc.doctype.common.duration.to"/></ac:parameter></ac:structured-macro>]]></xml>
      </value>
      <resource-bundle>
        <l10n>
          <description>
            The period addressed by this announcement.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Die Zeitspanne, die diese Mitteilung betrifft.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.service-announcement.source">
      <resource-bundle>
        <l10n>
          <name>Source</name>
          <description>
            Add links to documents that provide more information.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Quelle</name>
          <description>
            Fügen Sie Links auf Dokumente ein, die auf Entitäten verweisen, die
            Gegenstand dieser Arbeit sind.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.service-announcement.contact">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder</param>
          <param
              name="property"
              key="projectdoc.doctype.service-announcement.contact" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Contact</name>
          <description>
            Log a reference to a stakeholder, with whom stakeholders may get
            in touch for further information for this announcement.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Ansprechpartner</name>
          <description>
            Benennen Sie einen Stakeholder als Ansprechpartner für weitere
            Informationen zu dieser Ankündigung.
          </description>
        </l10n>
      </resource-bundle>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe the purpose of this announcement.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie den Zweck dieser Mitteilung.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.common.summary">
      <resource-bundle>
        <l10n>
          <description>
            Summarize the content of the announcement. This message is presented
            in a message box on landing pages.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Fassen Sie den Inhalt der Mitteilung kurz zusammen. Diese Nachricht
            wird auf Landing-Pages dominant dargestellt.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="release"/>
    <doctype-ref id="deployment"/>
    <doctype-ref id="it-service"/>
    <doctype-ref id="it-asset"/>
  </related-doctypes>

  <wizard template="minimal-params" form-id="announcement-form">
    <field template="name" />
    <field template="short-description" height="5" />
    <field><xml><![CDATA[      <div class="field-group">
          <label for="day-date">{getText('projectdoc.blueprint.form.label.date')}</label>
          <input id="day-date" class="aui-date-picker text" type="date" name="day-date" size="10" autocomplete="off">
      </div>]]></xml></field>
    <field template="target-location" />
    <param name="projectdoc-adjustVarValues-toDatePicker">day-date</param>
  </wizard>
</doctype>
