<?xml version='1.0'?>
<!--

    Copyright 2017-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="configuration-item-status"
  base-template="type"
  provide-type="none"
  category="reference">
  <resource-bundle>
    <l10n>
      <name plural="Configuration Item Statuses">Configuration Item Status</name>
      <description>
        Provide information on possible configuration item statuses.
      </description>
      <about>
        Add configuration item status to categorize systems, applications, and
        configuration items.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Configuration-Item-Status">Configuration-Item-Status</name>
      <description>
        Dokumentieren Sie Status-Informationen zu Configuration-Items.
      </description>
      <about>
        Status-Informationen kategorisieren Systeme, Applikationen und
        Configuration-Items.
      </about>
    </l10n>
  </resource-bundle>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe the configuration item status.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie den Status für Configuration-Items.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.configuration-item.items">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.configuration-item.items"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.configuration-item.items.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.common.doctype.homepage.link"/>|</ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.configuration-item.status"/>&gt; ~ (${<at:i18n at:key="projectdoc.doctype.common.name"/>},${<at:i18n at:key="projectdoc.doctype.common.shortName"/>})</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-classes">items-table, display-table, items</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Configuration Items</name>
          <intro>List of configuration items bound to this status.</intro>
        </l10n>
        <l10n locale="de">
          <name>Configuration-Items</name>
          <intro>
            Liste von Configuration-Items, die diesem Status zugeordnet sind.
          </intro>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="it-system" />
    <doctype-ref id="it-system-type" />
    <doctype-ref id="application" />
    <doctype-ref id="application-type" />
    <doctype-ref id="configuration-item" />
    <doctype-ref id="configuration-item-type" />
    <doctype-ref id="it-service-status" />
  </related-doctypes>
</doctype>
