<?xml version='1.0'?>
<!--

    Copyright 2017-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="change-impact"
  base-template="type"
  provide-type="none">
  <resource-bundle>
    <l10n>
      <name>Change Impact</name>
      <description>
        Add an impact to classify changes.
      </description>
      <about>
        Add an impact to classify and organize changes.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Auswirkungen">Auswirkung</name>
      <description>
        Fügen Sie eine Auswirkung für Änderungen hinzu.
      </description>
      <about>
        Erstellen Sie eine Auswirkung zur Klassifizierung von Änderungen.
      </about>
    </l10n>
  </resource-bundle>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe the impact for a change.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie die Auswirkung einer Änderung.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.change-impact.items">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.change-impact.items"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.change-impact.items.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.common.doctype.homepage.link"/>|</ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.change.impact"/>&gt; = [${<at:i18n at:key="projectdoc.doctype.common.name"/>}]</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-classes">items-table, display-table, items</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Changes</name>
          <intro>List of changes of this impact.</intro>
        </l10n>
        <l10n locale="de">
          <name>Changes</name>
          <intro>
            Liste von Änderungen, die dieser Auswirkung zugeordnet sind.
          </intro>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="change-priority" />
    <doctype-ref id="change-severity" />
    <doctype-ref id="change-urgency" />
    <doctype-ref id="change-theme" />
    <doctype-ref id="change-status" />
    <doctype-ref id="change-type" />
    <doctype-ref id="change" />
    <doctype-ref id="release" />
    <doctype-ref id="deployment" />
  </related-doctypes>
</doctype>
