<?xml version='1.0'?>
<!--

    Copyright 2017-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="trigger"
  base-template="standard"
  provide-type="standard-type"
  category="design">
  <resource-bundle>
    <l10n>
      <name>Trigger</name>
      <description>
        Document a triggers for processes or activities.
      </description>
      <about>
        Triggers affect processes and start activities. By specifying triggers
        in separate documents, multiple processes and activities can refer to
        them.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Auslöser">Auslöser</name>
      <description>
        Dokumentieren Sie Auslöser für Prozesse und Aktivitäten.
      </description>
      <about>
        Auslöser wirken auf Prozesse und starten Aktivitäten. Indem Sie Auslöser
        separat dokumentieren, können sie von verschiedenen Prozessen und
        Aktivitäten referenziert werden.
      </about>
      <type plural="Auslösertypen">Auslösertyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">trigger-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe the trigger in one to three paragraphs.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie den Auslöser in ein bis drei Paragraphen.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.trigger.processes">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.trigger.processes"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.trigger.processes.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">it-process</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.it-process.triggered-by"/>&gt; ~ (${<at:i18n at:key="projectdoc.doctype.common.name"/>},${<at:i18n at:key="projectdoc.doctype.common.shortName"/>})</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-classes">processes-table, display-table, processes</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Processes</name>
          <intro>List of processes triggered by this trigger.</intro>
        </l10n>
        <l10n locale="de">
          <name>Prozesse</name>
          <intro>
            Liste von Prozessen, die durch diese Auslöser ausgelöst werden.
          </intro>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.trigger.activities">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.trigger.activities"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.trigger.activities.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">it-activity</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.it-activity.triggered-by"/>&gt; ~ (${<at:i18n at:key="projectdoc.doctype.common.name"/>},${<at:i18n at:key="projectdoc.doctype.common.shortName"/>})</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-classes">activities-table, display-table, activities</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Activities</name>
          <intro>List of activities triggered by this trigger.</intro>
        </l10n>
        <l10n locale="de">
          <name>Aktivitäten</name>
          <intro>
            Liste von Aktivitäten, die durch diese Auslöser ausgelöst werden.
          </intro>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="it-service" />
    <doctype-ref id="it-process" />
    <doctype-ref id="it-activity" />
    <doctype-ref id="dataset" />
  </related-doctypes>
</doctype>
