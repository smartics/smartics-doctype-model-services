<?xml version='1.0'?>
<!--

    Copyright 2017-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="release"
  base-template="standard"
  provide-type="standard-type"
  category="operations">
  <resource-bundle>
    <l10n>
      <name>Release</name>
      <description>
        Document a release of a service.
      </description>
      <about>
        Releases document the changes that are applied to a published version
        of a service.
      </about>
    </l10n>
    <l10n locale="de">
      <name>Release</name>
      <description>
        Dokumentieren Sie ein Release eines Services.
      </description>
      <about>
        Releases dokumentieren die Änderungen an einem Service in Form von
        Änderungen (Changes) zu einer Version.
      </about>
      <type plural="Releasetypen">Releasetyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">release-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.release.it-assets">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">it-asset</param>
          <param
              name="property"
              key="projectdoc.doctype.release.it-assets" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
      <controls>mat:<![CDATA[<at:i18n at:key="projectdoc.doctype.it-asset.dependencies"/>]]>, mat:<![CDATA[<at:i18n at:key="projectdoc.doctype.release.previousRelease"/>-&gt;<at:i18n at:key="projectdoc.doctype.release.it-assets"/>]]></controls>
      <resource-bundle>
        <l10n>
          <name>Assets</name>
          <description>
            List the assets created and published by this release.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Assets</name>
          <description>
            Zählen Sie die Assets auf, die durch dieses Release erzeugt und
            veröffentlicht werden.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.release.version">
      <value><xml><![CDATA[<at:var at:name="projectdoc_doctype_release_version"/>]]></xml></value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <name>Version</name>
        </l10n>
        <l10n locale="de">
          <name>Version</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.release.date">
      <value><xml><![CDATA[<at:var at:name="day-date-picker" at:rawxhtml="true"/>]]></xml></value>
      <resource-bundle>
        <l10n>
          <name>Release Date</name>
        </l10n>
        <l10n locale="de">
          <name>Veröffentlichungsdatum</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.release.previousRelease">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">release</param>
          <param
              name="property"
              key="projectdoc.doctype.release.previousRelease" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Previous Release</name>
          <description>
            Specify a link to the previous release.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Vorgängerversion</name>
          <description>
            Geben Sie einen Verweis auf die Vorgängerversion des Releases an.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.release.nextRelease">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">release</param>
          <param
              name="property"
              key="projectdoc.doctype.release.nextRelease" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Next Release</name>
          <description>
            Specify a link to the next release.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Nachfolgerversion</name>
          <description>
            Geben Sie einen Verweis auf die Nachfolgerversion des Releases an.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.release.contact">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder</param>
          <param
              name="property"
              key="projectdoc.doctype.relase.contact" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Contact</name>
          <description>
            Log a reference to a stakeholder, with whom stakeholders may get
            in touch for further information for this release.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Ansprechpartner</name>
          <description>
            Benennen Sie einen Stakeholder als Ansprechpartner für dieses
            Release.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.sortKey">
      <value><xml><![CDATA[<at:var at:name="projectdoc_doctype_release_version.expandVersion"/>]]></xml></value>
      <controls>hide</controls>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe the purpose of this release. You may add
            also information about the product to help readers to determine, if
            this release is relevant for them.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie den Zweck dieses Releases. Sie können hier
            auch Informationen zu den Änderungen geben, die Lesern helfen
            herauszufinden, ob dieses Releases relevant für sie ist.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.common.summary">
      <resource-bundle>
        <l10n>
          <description>
            List the main features of this release and the most important
            information for users.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Listen Sie die wichtigsten Features dieses Releases und geben Sie
            die wesentlichsten Informationen für die Leser an.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.release.new-and-noteworthy">
      <resource-bundle>
        <l10n>
          <name>New and Noteworthy</name>
          <description>
            Describe the most interesting changes in more detail.
          </description>
          <intro>
            The following changes may be the most interesting of this release.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Neu und Wichtig</name>
          <description>
            Beschreiben Sie die wesentlichen Änderungen detaillierter.
          </description>
          <intro>
            Die folgenden Änderungen sind die wesentlichsten dieses Releases.
          </intro>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.release.requirements">
      <resource-bundle>
        <l10n>
          <name>Requirements</name>
          <description>
            List requirements for the new version of the product.
          </description>
          <intro>
            This release has the following requirements.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Anforderungen</name>
          <description>
            Listen Sie die Anforderungen, die die neue Version des Services
            stellt.
          </description>
          <intro>
            Dieses Release stellt die folgenden Anforderungen.
          </intro>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.release.breaking-changes">
      <xml><![CDATA[            <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.release.breaking-changes"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.release.breaking-changes.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">change</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-mode">unnumbered</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.change.id"/>+, <at:i18n at:key="projectdoc.doctype.common.name"/>-</ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.change.release-references"/>&gt;=[${<at:i18n at:key="projectdoc.doctype.common.name"/>}] AND $&lt;<at:i18n at:key="projectdoc.doctype.common.type"/>&gt;=[<at:i18n at:key="projectdoc.doctype.change.type.value.breaking"/>]</ac:parameter>
            <ac:parameter ac:name="render-id">list-of-breaking-changes</ac:parameter>
            <ac:parameter ac:name="render-classes">breaking-changes-table, display-table, breaking-changes</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Breaking Changes</name>
          <description>
            Describe changes that remove or break existing features that
            require the user's attention.
          </description>
          <intro>
            The following changes are incompatible with the previous release.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Inkompatible Änderungen</name>
          <description>
            Beschreiben Sie die Änderungen, die Features entfernen oder auf
            inkompatible Art ändern. Die Kenntnis dieser Änderungen sind für
            die Anwender wesentlich.
          </description>
          <intro>
            Die folgenden Änderungen sind mit der Vorgängerversion nicht
            kompatibel.
          </intro>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.release.known-issues">
      <resource-bundle>
        <l10n>
          <name>Known Issues</name>
          <description>
            List known issues with this release and provide workarounds and
            links to track the issues.
          </description>
          <intro>
            This release has the following issues.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Bekannte Probleme</name>
          <description>
            Listen sie bekannte Probleme dieses Releases und geben Sie Hinweise
            zu Workarounds und Verweise, um Problemmeldungen zu verfolgen.
          </description>
          <intro>
            Dieses Release hat keine Lösung für die folgenden Probleme.
          </intro>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.release.list-of-changes">
      <xml><![CDATA[            <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.release.list-of-changes"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.release.list-of-changes.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-content-marker">
            <ac:parameter ac:name="id">list-of-changes-buttons</ac:parameter>
            <ac:parameter ac:name="required-space-properties">!pretend-being-a=printer</ac:parameter>
            <ac:parameter ac:name="consider-as-empty">true</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="projectdoc-link-wiki">
                  <ac:parameter ac:name="request-parameters">list-of-changes:select=<at:i18n at:key="projectdoc.doctype.change.id"/>+, <at:i18n at:key="projectdoc.doctype.common.name"/>-, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.common.description"/>, <at:i18n at:key="projectdoc.doctype.change.status"/>||&amp;list-of-changes:render-mode=*s&amp;list-of-changes:render-heading-as-link=true</ac:parameter>
                  <ac:parameter ac:name="render-space-ref-css">false</ac:parameter>
                  <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.doctype.release.list-of-changes.button.fullView.label"/></ac:parameter>
                  <ac:parameter ac:name="tooltip"><at:i18n at:key="projectdoc.doctype.release.list-of-changes.button.fullView.tooltip"/></ac:parameter>
                  <ac:parameter ac:name="request-toggle-label"><at:i18n at:key="projectdoc.doctype.release.list-of-changes.button.overview.label"/></ac:parameter>
                  <ac:parameter ac:name="request-toggle-tooltip"><at:i18n at:key="projectdoc.doctype.release.list-of-changes.button.overview.tooltip"/></ac:parameter>
                  <ac:parameter ac:name="render-no-docs-css">false</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">change</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.change.id"/>+, <at:i18n at:key="projectdoc.doctype.common.name"/>-, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.change.status"/>|</ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.change.release-references"/>&gt;=[${<at:i18n at:key="projectdoc.doctype.common.name"/>}]</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.change.id"/>, <at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-id">list-of-changes</ac:parameter>
            <ac:parameter ac:name="render-classes">changes-table, display-table, changes</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>List of Changes</name>
          <description>
            List all changes of this release.
          </description>
          <intro>
            The complete list of changes for this release.
          </intro>
          <label key="projectdoc.doctype.release.list-of-changes.button.fullView.label">Full view</label>
          <label key="projectdoc.doctype.release.list-of-changes.button.fullView.tooltip">Mehr Informationen zu den Änderungen auf dieser Seite anzeigen.</label>
          <label key="projectdoc.doctype.release.list-of-changes.button.overview.label">Überblickssicht</label>
          <label key="projectdoc.doctype.release.list-of-changes.button.overview.tooltip">Übersicht über Änderungen auf dieser Seite anzeigen.</label>
        </l10n>
        <l10n locale="de">
          <name>Liste der Änderungen</name>
          <description>
            Listen Sie alle Änderungen in diesem Release.
          </description>
          <intro>
            Die vollständige Liste der Änderungen in diesem Release.
          </intro>
          <label key="projectdoc.doctype.release.list-of-changes.button.fullView.label">Vollansicht</label>
          <label key="projectdoc.doctype.release.list-of-changes.button.fullView.tooltip">Mehr Informationen zu den Änderungen auf dieser Seite anzeigen.</label>
          <label key="projectdoc.doctype.release.list-of-changes.button.overview.label">Überblickssicht</label>
          <label key="projectdoc.doctype.release.list-of-changes.button.overview.tooltip">Übersicht über Änderungen auf dieser Seite anzeigen.</label>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.common.resources">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.resources"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-table-merger-macro">
            <ac:rich-text-body>
              <ul>
            <li><ac:structured-macro ac:name="projectdoc-link-wiki">
                <ac:parameter ac:name="page">projectdoc.content.release.home.title</ac:parameter>
                <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.doctype.release.releases-archive.label"/></ac:parameter>
              </ac:structured-macro></li>
            <li><ac:structured-macro ac:name="projectdoc-link-wiki">
                <ac:parameter ac:name="page">projectdoc.content.faq.home.title</ac:parameter>
              </ac:structured-macro></li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <label key="projectdoc.doctype.release.releases-archive.label">Releases Archive</label>
        </l10n>
        <l10n locale="de">
          <label key="projectdoc.doctype.release.releases-archive.label">Archiv der Releases</label>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="service-announcement" />
    <doctype-ref id="change" />
    <doctype-ref id="it-asset" />
    <doctype-ref id="it-service" />
    <doctype-ref id="deployment" />
  </related-doctypes>

  <wizard template="minimal-params" form-id="release-form">
    <field template="name" />
    <field template="short-description" height="3" />
    <field><xml><![CDATA[      <div class="field-group">
          <label for="projectdoc_doctype_release_version">{getText('projectdoc.doctype.release.version')}</label>
          <input id="projectdoc_doctype_release_version" class="text long-field" type="text" name="projectdoc_doctype_release_version" autocomplete="off" placeholder="{getText('projectdoc.doctype.release.version.placeholder')}">
      </div>
      <div class="field-group">
          <label for="day-date">{getText('projectdoc.blueprint.form.label.date')}</label>
          <input id="day-date" class="aui-date-picker text" type="date" name="day-date" size="10" autocomplete="off">
      </div>]]></xml></field>
    <field template="target-location" />
    <param name="projectdoc-adjustVarValues-toDatePicker">day-date</param>
    <param name="projectdoc-adjustVarValues-expandVersion">projectdoc_doctype_release_version</param>
  </wizard>
</doctype>
