<?xml version='1.0'?>
<!--

    Copyright 2017-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="deployment"
  base-template="standard"
  provide-type="standard-type"
  category="operations">
  <resource-bundle>
    <l10n>
      <name>Deployment</name>
      <description>
        Document a deployment for users of the related services and systems.
      </description>
      <about>
        Deployments provide information about updates on services and systems
        to users.
      </about>
    </l10n>
    <l10n locale="de">
      <name>Deployment</name>
      <description>
        Dokumentieren Sie ein Deployment auf ein System.
      </description>
      <about>
        Deployments dokumentieren Änderungen an Services und Systemen, die für
        deren Nutzer relevant sind.
      </about>
      <type plural="Deploymenttypen">Deploymenttyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">deployment-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>

    <property key="projectdoc.doctype.deployment.it-services">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">it-service</param>
          <param
              name="property"
              key="projectdoc.doctype.deployment.it-services" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <name>IT Services</name>
          <description>
            List the services that are affected by this deployment. Take into
            account that not all services relying on a affected system may
            themselves be affected.
          </description>
        </l10n>
        <l10n locale="de">
          <name>IT-Services</name>
          <description>
            Zählen Sie die durch dieses Deployment betroffenen Services auf.
            Beachten Sie dabei, dass nicht notwendigerweise alle Services, die
            auf betroffenen Systemen arbeiten, selbst betroffen sein müssen.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.deployment.it-systems">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">it-system</param>
          <param
            name="property"
            key="projectdoc.doctype.deployment.it-systems" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <name>IT Systems</name>
          <description>
            List the systems that are updated by this deployment.
          </description>
        </l10n>
        <l10n locale="de">
          <name>IT-Systeme</name>
          <description>
            Zählen Sie die durch dieses Deployment aktualisierten Systeme.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.deployment.assets">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">it-asset</param>
          <param
              name="property"
              key="projectdoc.doctype.deployment.assets" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <name>Assets</name>
          <description>
            List the assets that are deployed to the referenced systems and
            take affect on the referenced services..
          </description>
        </l10n>
        <l10n locale="de">
          <name>Assets</name>
          <description>
            Zählen Sie die Assets auf, die auf die referenzierten Systeme
            deployt werden und durch die die referenzierten Services betroffen
            sind.
          </description>
        </l10n>
      </resource-bundle>
    </property>

    <property key="projectdoc.doctype.deployment.deploy-date">
      <resource-bundle>
        <l10n>
          <name>Date</name>
          <description>
            Log the date of the deployment that made the product available to
            its users.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Datum</name>
          <description>
            Spezifizieren Sie das Datum des Deployments.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.deployment.available-until-date">
      <resource-bundle>
        <l10n>
          <name>Available until</name>
          <description>Log the date the availability of the product ends.</description>
        </l10n>
        <l10n locale="de">
          <name>Verfügbar bis</name>
          <description>Spezifizieren Sie das Datum ab dem das Deployment nicht mehr verfügbar ist.</description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.deployment.contact">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder</param>
          <param
              name="property"
              key="projectdoc.doctype.deployment.contact" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Contact</name>
          <description>
            Log a reference to a stakeholder, with whom stakeholders may get
            in touch for further information for this deployment.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Ansprechpartner</name>
          <description>
            Benennen Sie einen Stakeholder als Ansprechpartner für dieses
            Release.
          </description>
        </l10n>
      </resource-bundle>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe the context of the deployment for users of the related
            services and systems.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie den Kontext des Deployments für die Nutzer der
            betroffenen Services and Systeme.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.deployment.list-of-services">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.deployment.list-of-services"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.deployment.list-of-services.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-tour-by-property-macro">
            <ac:parameter ac:name="render-id">services</ac:parameter>
            <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.deployment.it-services"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-classes">services-table, tour-by-property, services</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>List of Services</name>
          <description>
            Automatically generated list of all services with an impact by this
            deployment.
          </description>
          <intro>
            The complete list of services that are affected by this deployment.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Liste der Services</name>
          <description>
            Automatisch generierte Liste aller Services, die durch dieses
            Deployment beeinflusst werden.
          </description>
          <intro>
            Die vollständige Liste der Services, die durch dieses Deployment
            beeinflusst werden.
          </intro>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.deployment.list-of-systems">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.deployment.list-of-systems"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.deployment.list-of-systems.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-tour-by-property-macro">
            <ac:parameter ac:name="render-id">systems</ac:parameter>
            <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.deployment.it-systems"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-classes">systems-table, tour-by-property, systems</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>List of Systems</name>
          <description>
            Automatically generated list of all systems that are part of this
            deployment.
          </description>
          <intro>
            The complete list of systems that are changed by this deployment.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Liste der Systeme</name>
          <description>
            Automatisch generierte Liste aller Systeme, die mit diesem
            Deployment neu bespielt werden.
          </description>
          <intro>
            Die vollständige Liste der Systeme, die Teil dieses Deployments
            sind.
          </intro>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.deployment.list-of-assets">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.deployment.list-of-assets"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.deployment.list-of-assets.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-tour-by-property-macro">
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.it-asset.version"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.it-asset.release"/></ac:parameter>
            <ac:parameter ac:name="render-id">assets</ac:parameter>
            <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.deployment.assets"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-classes">assets-table, tour-by-property, assets</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>List of Assets</name>
          <description>
            Automatically generated list of all assets related to this
            deployment that are relevant to the users of the services and
            systems.
          </description>
          <intro>
            The list of assets that are installed or required for the
            installation by this deployment.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Liste der Assets</name>
          <description>
            Automatisch generierte Liste aller Assets, die durch dieses
            Deployment installiert oder für die Installation benötigt wurden
            und für die Nutzer der Services und Systeme von Relevanz sind.
          </description>
          <intro>
            Die Liste der Assets, die mit diesem Deployment installiert
            oder für die Installation benötigt wurden.
          </intro>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="deployment-record" />
    <doctype-ref id="it-service" />
    <doctype-ref id="it-system" />
    <doctype-ref id="it-asset" />
    <doctype-ref id="release" />
    <doctype-ref id="stakeholder" />
  </related-doctypes>
</doctype>
