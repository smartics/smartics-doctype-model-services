<?xml version='1.0'?>
<!--

    Copyright 2017-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="it-activity"
  base-template="standard"
  provide-type="standard-type"
  category="strategy">
  <resource-bundle>
    <l10n>
      <name plural="IT Activities">IT Activity</name>
      <description>
        Define activities for your processes.
      </description>
      <about>
        IT Activities define activities for processes.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="IT-Aktivitäten">IT-Aktivität</name>
      <description>
        Beschreiben Sie Aktivitäten ihrer Prozesse.
      </description>
      <about>
        Ein Prozess kann in einzelnen Aktivitäten zerlegt werden. Diese
        Aktivitäten werden separat beschrieben.
      </about>
      <type plural="IT-Aktivitätstypen">IT-Aktivitätstyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.it-activity.type-parent">
      <value>
        <macro name="projectdoc-transclusion-property-display">
          <param name="property-name" key="projectdoc.doctype.common.parent" />
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Superordinate Activity</name>
        </l10n>
        <l10n locale="de">
          <name>Übergeordnete Aktivität</name>
        </l10n>
      </resource-bundle>
    </property>

    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">it-activity-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.it-activity.process">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">it-process</param>
          <param
            name="property"
            key="projectdoc.doctype.it-activity.process" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Process</name>
        </l10n>
        <l10n locale="de">
          <name>Prozess</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.lifecycle-phase.template.title">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">lifecycle-phase</param>
          <param
              name="property"
              key="projectdoc.lifecycle-phase.template.title" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
          <param name="property-matcher"><![CDATA[$[<at:i18n at:key="projectdoc.doctype.lifecycle-phase.lifecycle"/>]="<at:i18n at:key="projectdoc.doctype.it-activity.lifecycle-phase.defaultId"/>"]]></param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.it-activity.participants">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">role</param>
          <param
              name="property"
              key="projectdoc.doctype.it-activity.participants" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Participants</name>
        </l10n>
        <l10n locale="de">
          <name>Teilnehmer</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.it-activity.triggered-by">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">trigger</param>
          <param
            name="property"
            key="projectdoc.doctype.it-activity.triggered-by" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <name>Triggered by</name>
        </l10n>
        <l10n locale="de">
          <name>Ausgelöst durch</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.it-activity.input-datasets">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">dataset</param>
          <param
              name="property"
              key="projectdoc.doctype.it-activity.input-datasets" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <name>Input Datasets</name>
        </l10n>
        <l10n locale="de">
          <name>Eingabedaten</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.it-activity.output-datasets">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">dataset</param>
          <param
              name="property"
              key="projectdoc.doctype.it-activity.output-datasets" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <name>Output Datasets</name>
        </l10n>
        <l10n locale="de">
          <name>Ausgabedaten</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.it-activity.contact">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder, role, person</param>
          <param name="property" key="projectdoc.doctype.it-activity.contact" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n><name>Contact</name></l10n>
        <l10n locale="de"><name>Ansprechpartner</name></l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.it-activity.related-activities">
      <value>
        <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">it-activity</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.it-activity.related-activities"/></ac:parameter>
                      <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                    </ac:structured-macro>]]></xml>
      </value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <name>Related Activities</name>
          <description>
            Add links to closely related activities. Activities will be
            rendered within a document section.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Verwandte Aktivitäten</name>
          <description>
            Fügen Sie Links auf eng verwandte Aktivitäten hinzu. Die Aktivitäten
            werden in einem Abschnitt des Dokuments angezeigt.
          </description>
        </l10n>
      </resource-bundle>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe the purpose of the activity.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie das Ziel und den Zweck der Aktivität.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.it-activity.triggers">
      <xml><![CDATA[          <ac:structured-macro ac:name="projectdoc-section">
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.it-activity.triggers"/></ac:parameter>
            <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.it-activity.triggers.intro"/></ac:parameter>
            <ac:rich-text-body>
              <ac:structured-macro ac:name="projectdoc-tour-by-property-macro">
                <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.it-activity.triggered-by"/></ac:parameter>
                <ac:parameter ac:name="render-counter-column">true</ac:parameter>
                <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                <ac:parameter ac:name="render-classes">triggers-table, tour-by-property, triggers</ac:parameter>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>
      ]]></xml>
      <resource-bundle>
        <l10n>
          <name>Triggers</name>
          <description>
            List the triggers that starts this activity.
          </description>
          <intro>
            List of triggers that start this activity.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Auslöser</name>
          <description>
            Zählen Sie die Auslöser auf, die diese Aktivität startet.
          </description>
          <intro>
            Liste von Auslösern, die diese Aktivität starten.
          </intro>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.it-activity.input">
      <xml><![CDATA[          <ac:structured-macro ac:name="projectdoc-section">
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.it-activity.input"/></ac:parameter>
            <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.it-activity.input.intro"/></ac:parameter>
            <ac:rich-text-body>
              <ac:structured-macro ac:name="projectdoc-tour-by-property-macro">
                <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.it-activity.input-datasets"/></ac:parameter>
                <ac:parameter ac:name="render-counter-column">true</ac:parameter>
                <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                <ac:parameter ac:name="render-classes">tour-by-property-table, tour-by-property, tour-by-property</ac:parameter>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Input</name>
          <description>
            Describe the input datasets for this activity.
          </description>
          <intro>
            List of inputs to this activity.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Eingaben</name>
          <description>
            Beschreiben Sie die Eingabedaten, die für den Start der
            Aktivität benötigt werden.
          </description>
          <intro>
            Liste von Eingaben für die Aktivität.
          </intro>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.it-activity.output">
      <xml><![CDATA[          <ac:structured-macro ac:name="projectdoc-section">
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.it-activity.output"/></ac:parameter>
            <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.it-activity.output.intro"/></ac:parameter>
            <ac:rich-text-body>
              <ac:structured-macro ac:name="projectdoc-tour-by-property-macro">
                <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.it-activity.output-datasets"/></ac:parameter>
                <ac:parameter ac:name="render-counter-column">true</ac:parameter>
                <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                <ac:parameter ac:name="render-classes">output-table, tour-by-property, output</ac:parameter>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Output</name>
          <description>
            Describe the output datasets for this activity.
          </description>
          <intro>
            List of outputs from this activity.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Ausgaben</name>
          <description>
            Beschreiben Sie die Ausgabedaten, die nach erfolgreicher
            Abarbeitung der Aktivität erzeugt wurden.
          </description>
          <intro>
            Liste von Ausgaben der Aktivität.
          </intro>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.it-activity.work-instructions">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.it-activity.work-instructions"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">work-instruction</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.work-instruction.target"/>&gt;=[${<at:i18n at:key="projectdoc.doctype.common.name"/>}]</ac:parameter>
            <ac:parameter ac:name="render-classes">work-instructions-table, display-table, work-instructions</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Work Instructions</name>
          <description>
            In case the activity is not detailed with a procedure document,
            attach the work instructions to this activity.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Arbeitsanweisungen</name>
          <description>
            Sofern die Aktivität nicht durch ein Verfahren beschrieben wird,
            können die Arbeitsanweisungen auch direkt mit der Aktivität
            verknüpft werden.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.it-activity.list-of-related-activities">
      <xml><![CDATA[          <ac:structured-macro ac:name="projectdoc-section">
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.it-activity.list-of-related-activities"/></ac:parameter>
            <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.it-activity.list-of-related-activities.intro"/></ac:parameter>
            <ac:rich-text-body>
              <ac:structured-macro ac:name="projectdoc-tour-by-property-macro">
                <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.it-activity.related-activities"/></ac:parameter>
                <ac:parameter ac:name="render-counter-column">true</ac:parameter>
                <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                <ac:parameter ac:name="render-classes">activities-table, tour-by-property, activities</ac:parameter>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>
      ]]></xml>
      <resource-bundle>
        <l10n>
          <name>List of related Activities</name>
          <intro>This activity is closely related to the following activities.</intro>
        </l10n>
        <l10n locale="de">
          <name>Liste der verwandte Aktivitäten</name>
          <intro>Diese Aktivität ist mit den folgenden Aktivitäten eng verwandt.</intro>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.it-activity.reports">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.it-activity.reports"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.it-activity.reports.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">report</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.itsm.report.date"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.itsm.report.execution"/>&gt;=[${<at:i18n at:key="projectdoc.doctype.common.name"/>}] AND $[<at:i18n at:key="projectdoc.doctype.itsm.report.date"/>]: [* TO *]</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-classes">reports-table, display-table, process-documentation</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.itsm.report.date"/>§,<at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Reports</name>
          <intro>
            List of published reports created by this activity.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Berichte</name>
          <intro>
            Liste von veröffentlichten Berichten, die durch diese Aktivität
            erstellt wurden.
          </intro>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="it-process" />
    <doctype-ref id="it-procedure" />
    <doctype-ref id="it-function" />
    <doctype-ref id="work-instruction" />
    <doctype-ref id="it-service" />
    <doctype-ref id="stakeholder" />
    <doctype-ref id="role" />
    <doctype-ref id="resource" />
    <doctype-ref id="trigger" />
  </related-doctypes>
</doctype>
