<?xml version='1.0'?>
<!--

    Copyright 2017-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="dataset"
  base-template="standard"
  provide-type="standard-type"
  category="design">
  <resource-bundle>
    <l10n>
      <name>Dataset</name>
      <description>
        Document a dataset for input and output.
      </description>
      <about>
        Document a dataset used as input and produced as output by processes.
        Allows to apply privacy protection information.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Datensätze">Datensatz</name>
      <description>
        Dokumentieren Sie Datensätze.
      </description>
      <about>
        Dokumentieren Sie einen Datensatz, der als Eingabe oder Ausgabe eines
        Prozesses verwendet wird.
      </about>
      <type plural="Datensatztypen">Datensatztyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">dataset-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe the dataset in one to three paragraphs.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie den Datensatz in ein bis drei Paragraphen.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.dataset.elements">
      <resource-bundle>
        <l10n>
          <name>Elements</name>
          <description>
            List the elements of the dataset, if there is more than one.
            Describe each element in its own section or use a tabular
            representation.
          </description>
          <intro>The dataset contains the following elements.</intro>
        </l10n>
        <l10n locale="de">
          <name>Elemente</name>
          <description>
            Zählen Sie die Elemente des Datensatzes auf, falls es er aus mehr
            als einem Datum besteht. Beschreiben Sie jedes Element in einem
            eigenen Abschnitt oder verwenden Sie für die Darstellung eine
            Tabelle.
          </description>
          <intro>Der Datensatz umfasst die folgenden Elemente.</intro>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.dataset.processes-output">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.dataset.processes-output"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.dataset.processes-output.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">it-process</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.it-process.output-datasets"/>&gt; ~ (${<at:i18n at:key="projectdoc.doctype.common.name"/>},${<at:i18n at:key="projectdoc.doctype.common.shortName"/>})</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-classes">processes-output-table, display-table, processes-output</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Output for Processes</name>
          <intro>List of processes that produce this dataset.</intro>
        </l10n>
        <l10n locale="de">
          <name>Ausgabe für Prozesse</name>
          <intro>
            Liste von Prozessen, die diesen Datensatz erzeugen.
          </intro>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.dataset.processes-input">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.dataset.processes-input"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.dataset.processes-input.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">it-process</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.it-process.input-datasets"/>&gt; ~ (${<at:i18n at:key="projectdoc.doctype.common.name"/>},${<at:i18n at:key="projectdoc.doctype.common.shortName"/>})</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-classes">processes-input-table, display-table, processes-input</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Input for Processes</name>
          <intro>List of processes this dataset is expected as input.</intro>
        </l10n>
        <l10n locale="de">
          <name>Eingabe für Prozesse</name>
          <intro>
            Liste von Prozessen, die diesen Datensatz als Eingabe erwarten.
          </intro>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.dataset.extensions">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.dataset.extensions"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.dataset.extensions.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-transclude-documents-macro">
            <ac:parameter ac:name="where">+$[<at:i18n at:key="projectdoc.doctype.common.doctype-extension"/>]: [* TO *]</ac:parameter>
            <ac:parameter ac:name="taget-heading-level">*</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-classes">extensions-table, transclude-documents, extensions</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Extensions</name>
          <intro>
            More information on this dataset.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Erweiterungen</name>
          <description>
            Weitere Informationen zu diesem Datensatz.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="it-service" />
    <doctype-ref id="it-process" />
    <doctype-ref id="it-activity" />
    <doctype-ref id="trigger" />
  </related-doctypes>
</doctype>
