<?xml version="1.0"?>
<!--

    Copyright 2017-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->

<!--suppress UnresolvedMavenProperty -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>de.smartics.atlassian.confluence</groupId>
  <artifactId>smartics-doctype-model-services</artifactId>
  <version>7.0.1-SNAPSHOT</version>
  <packaging>jar</packaging>

  <name>Service Management Model</name>
  <description>
    projectdoc models for managing services and systems.
  </description>

  <inceptionYear>2017</inceptionYear>

  <organization>
    <name>smartics</name>
    <url>http://www.smartics.de/</url>
  </organization>

  <licenses>
    <license>
      <name>The Apache Software License, Version 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0</url>
      <distribution>repo</distribution>
      <comments>Copyright 2017-2025 Kronseder &amp; Reiner GmbH, smartics</comments>
    </license>
  </licenses>

  <developers>
    <developer>
      <id>robert.reiner</id>
      <name>Robert Reiner</name>
      <url>https://www.smartics.de/go/robertreiner</url>
      <organization>Kronseder &amp; Reiner GmbH, smartics</organization>
      <organizationUrl>http://www.smartics.de/</organizationUrl>
    </developer>
    <developer>
      <id>anton.kronseder</id>
      <name>Anton Kronseder</name>
      <url>https://www.smartics.de/go/antonkronseder</url>
      <organization>Kronseder &amp; Reiner GmbH, smartics</organization>
      <organizationUrl>http://www.smartics.de/</organizationUrl>
    </developer>
  </developers>

  <scm>
    <connection>scm:git:https://bitbucket.org/smartics/${project.artifactId}</connection>
    <developerConnection>
      scm:git:https://bitbucket.org/smartics/${project.artifactId}
    </developerConnection>
    <url>https://bitbucket.org/smartics/${project.artifactId}</url>
    <tag>HEAD</tag>
  </scm>

  <issueManagement>
    <system>jira</system>
    <url>https://www.smartics.eu/jira/projects/SERVICES</url>
  </issueManagement>

  <distributionManagement>
    <repository>
      <id>public</id>
      <name>public smartics release repository</name>
      <url>dav:https://nexus.smartics.eu/nexus/content/repositories/public</url>
    </repository>
    <snapshotRepository>
      <uniqueVersion>false</uniqueVersion>
      <id>public-snapshot</id>
      <name>public smartics snapshot repository</name>
      <url>
        dav:https://nexus.smartics.eu/nexus/content/repositories/public-snapshot
      </url>
    </snapshotRepository>
  </distributionManagement>

  <properties>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

    <doctype-maven-plugin.version>3.2.2-SNAPSHOT</doctype-maven-plugin.version>
    <smartics-projectdoc-confluence.version>7.2.1-SNAPSHOT</smartics-projectdoc-confluence.version>
    <smartics-projectdoc-confluence.version-space-core>20.0.1-SNAPSHOT</smartics-projectdoc-confluence.version-space-core>

    <!-- ... Maven Plugins ................................................. -->
    <maven-clean-plugin.version>3.3.2</maven-clean-plugin.version>
    <maven-site-plugin.version>3.4</maven-site-plugin.version>
    <maven-dependency-plugin.version>3.6.1</maven-dependency-plugin.version>
    <maven-resources-plugin.version>3.3.1</maven-resources-plugin.version>
    <maven-deploy-plugin.version>3.1.1</maven-deploy-plugin.version>
    <maven-jar-plugin.version>3.3.0</maven-jar-plugin.version>
    <maven-release-plugin.version>3.0.1</maven-release-plugin.version>
    <maven-install-plugin.version>3.1.1</maven-install-plugin.version>
    <maven-scm-plugin.version>2.0.1</maven-scm-plugin.version>
    <maven-toolchains-plugin.version>3.1.0</maven-toolchains-plugin.version>
    <maven-surefire-plugin.version>2.18.1</maven-surefire-plugin.version>

    <license-maven-plugin.version>4.6</license-maven-plugin.version>

    <buildmetadata-maven-plugin.version>1.7.0</buildmetadata-maven-plugin.version>
    <apptools-maven-plugin.version>0.12.1</apptools-maven-plugin.version>
  </properties>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <artifactId>maven-clean-plugin</artifactId>
          <version>${maven-clean-plugin.version}</version>
        </plugin>
        <plugin>
          <artifactId>maven-dependency-plugin</artifactId>
          <version>${maven-dependency-plugin.version}</version>
        </plugin>
        <plugin>
          <artifactId>maven-resources-plugin</artifactId>
          <version>${maven-resources-plugin.version}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>${maven-deploy-plugin.version}</version>
          <dependencies>
            <dependency>
              <groupId>org.apache.maven.wagon</groupId>
              <artifactId>wagon-webdav-jackrabbit</artifactId>
              <version>3.5.3</version>
            </dependency>
          </dependencies>
        </plugin>
        <plugin>
          <artifactId>maven-jar-plugin</artifactId>
          <version>${maven-jar-plugin.version}</version>
          <configuration>
            <archive>
              <index>true</index>
              <manifest>
                <addDefaultImplementationEntries>true
                </addDefaultImplementationEntries>
                <addDefaultSpecificationEntries>true
                </addDefaultSpecificationEntries>
              </manifest>
              <manifestEntries>
                <Implementation-URL>${project.url}</Implementation-URL>
                <Built-OS>${os.name} / ${os.arch} / ${os.version}</Built-OS>
                <Maven-Version>${build.maven.version}</Maven-Version>
                <Java-Version>${java.version}</Java-Version>
                <Java-Vendor>${java.vendor}</Java-Vendor>
              </manifestEntries>
            </archive>
          </configuration>
        </plugin>
        <plugin>
          <artifactId>maven-install-plugin</artifactId>
          <version>${maven-install-plugin.version}</version>
        </plugin>
        <plugin>
          <artifactId>maven-release-plugin</artifactId>
          <version>${maven-release-plugin.version}</version>
          <configuration>
            <useReleaseProfile>false</useReleaseProfile>
            <arguments>-P release-doctype-add-on</arguments>
          </configuration>
        </plugin>
        <plugin>
          <artifactId>maven-scm-plugin</artifactId>
          <version>${maven-scm-plugin.version}</version>
          <configuration>
            <connectionType>developerConnection</connectionType>
          </configuration>
        </plugin>
        <!--
        <plugin>
          <groupId>org.eclipse.m2e</groupId>
          <artifactId>lifecycle-mapping</artifactId>
          <version>1.0.0</version>
          <configuration>
            <lifecycleMappingMetadata>
              <pluginExecutions>
                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>de.smartics.maven.plugin</groupId>
                    <artifactId>buildmetadata-maven-plugin</artifactId>
                    <versionRange>[0.0.0,)</versionRange>
                    <goals>
                      <goal>provide-buildmetadata</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore />
                  </action>
                </pluginExecution>
                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>de.smartics.maven.plugin</groupId>
                    <artifactId>doctype-maven-plugin</artifactId>
                    <versionRange>[0.0.0.,)</versionRange>
                    <goals>
                      <goal>create</goal>
                      <goal>generate</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore />
                  </action>
                </pluginExecution>
              </pluginExecutions>
            </lifecycleMappingMetadata>
          </configuration>
        </plugin> -->

        <plugin>
          <!-- Usage: mvn apptools:deploy -PLOCAL -->
          <groupId>de.smartics.maven.plugin</groupId>
          <artifactId>apptools-maven-plugin</artifactId>
          <version>${apptools-maven-plugin.version}</version>
          <configuration>
            <sourceFolder>
              ${basedir}/target/smartics-doctype-addon-services/target
            </sourceFolder>
            <includes>
              <include>smartics-doctype-addon-services$</include>
            </includes>
            <acceptedFilenameExtensions>
              <extension>jar</extension>
            </acceptedFilenameExtensions>

            <serverId>${projectdoc.apptools.remote.server.id}</serverId>
            <serverUrl>${projectdoc.apptools.remote.server.url}</serverUrl>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>

    <plugins>
      <plugin>
        <groupId>de.smartics.maven.plugin</groupId>
        <artifactId>buildmetadata-maven-plugin</artifactId>
        <version>${buildmetadata-maven-plugin.version}</version>
        <executions>
          <execution>
            <phase>initialize</phase>
            <goals>
              <goal>provide-buildmetadata</goal>
            </goals>
          </execution>
        </executions>
       <configuration>
          <buildDatePattern>dd.MM.yyyy HH:mm:ss</buildDatePattern>
          <addScmInfo>false</addScmInfo>
        </configuration>
      </plugin>

      <plugin>
        <groupId>com.mycila</groupId>
        <artifactId>license-maven-plugin</artifactId>
        <version>${license-maven-plugin.version}</version>
        <executions>
          <execution>
            <goals>
              <goal>check</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <strictCheck>true</strictCheck>
          <properties>
            <year>${build.copyright.year}</year>
          </properties>
          <mapping>
            <vm>XML_STYLE</vm>
          </mapping>
          <licenseSets>
            <licenseSet>
              <header>src/etc/license/header.txt</header>
              <headerDefinitions>
                <headerDefinition>src/etc/license/javadoc.xml</headerDefinition>
              </headerDefinitions>
              <excludes>
                <exclude>**/COPYRIGHT.txt</exclude>
                <exclude>**/LICENSE</exclude>
                <exclude>**/LICENSE.txt</exclude>
                <exclude>**/LICENSE-*</exclude>
                <exclude>**/javadoc.xml</exclude>
                <exclude>**/header.txt</exclude>
                <exclude>**/pom.xml.releaseBackup</exclude>
                <exclude>.gitignore</exclude>
                <exclude>**/*.md</exclude>
                <exclude>**/*.soy</exclude>
                <exclude>src/main/resources/projectdoc-models/patch/**</exclude>
                <exclude>src/main/resources/projectdoc-models/tools/*/*.*</exclude>
                <exclude>src/main/resources/projectdoc-models/doctypes/*/*.*</exclude>
                <exclude>**/.idea/**</exclude>
              </excludes>
            </licenseSet>
          </licenseSets>
        </configuration>
      </plugin>

      <plugin>
        <artifactId>maven-resources-plugin</artifactId>
        <executions>
          <execution>
            <id>copy-resources</id>
            <phase>compile</phase>
            <goals>
              <goal>copy-resources</goal>
            </goals>
            <configuration>
              <outputDirectory>${project.build.outputDirectory}/META-INF
              </outputDirectory>
              <resources>
                <resource>
                  <directory>${basedir}</directory>
                  <includes>
                    <include>COPYRIGHT.txt</include>
                    <include>LICENSE-*</include>
                  </includes>
                  <filtering>false</filtering>
                </resource>
              </resources>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>de.smartics.maven.plugin</groupId>
        <artifactId>doctype-maven-plugin</artifactId>
        <version>${doctype-maven-plugin.version}</version>
        <executions>
          <execution>
            <id>generate-doctype</id>
            <phase>generate-resources</phase>
            <goals>
              <goal>create</goal>
              <goal>generate</goal>
            </goals>
          </execution>
          <execution>
            <id>build-orb</id>
            <phase>package</phase>
            <goals>
              <goal>build</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <readProjectSettings>false</readProjectSettings>
          <verbose>true</verbose>

          <shortId>services</shortId>
          <projectName>projectdoc for Service Management</projectName>
          <projectDescription>
            projectdoc Blueprints for managing services and systems.
          </projectDescription>
          <organizationSignature>
            Kronseder &amp; Reiner GmbH, smartics
          </organizationSignature>
          <dataCenterApproved>true</dataCenterApproved>

          <createExamples>false</createExamples>
          <generateCategoryTree>true</generateCategoryTree>

          <packagePrefix>de.smartics.projectdoc</packagePrefix>
          <modelsFolder>/src/main/resources/projectdoc-models</modelsFolder>
          <groupId>de.smartics.atlassian.confluence</groupId>
          <artifactIdPrefix>smartics-doctype-addon-</artifactIdPrefix>
          <projectVersion>${project.version}</projectVersion>

          <confluenceCompatibilityMin>9.1.1</confluenceCompatibilityMin>
          <confluenceCompatibilityMax>9.3.2</confluenceCompatibilityMax>

          <pluginArtifacts>
            <pluginArtifact>
              <groupId>de.smartics.atlassian.confluence</groupId>
              <artifactId>smartics-projectdoc-confluence-space-core</artifactId>
              <version>${smartics-projectdoc-confluence.version-space-core}
              </version>
            </pluginArtifact>
          </pluginArtifacts>

          <references>
            <reference>
              <name>addon-documentation</name>
              <type>confluence</type>
              <locator>http://www.smartics.eu/confluence/display/PDAC1</locator>
            </reference>
          </references>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <profiles>
    <profile>
      <id>release-doctype-add-on</id>
      <properties>
        <project.atlassian.plugin.version>${project.version}</project.atlassian.plugin.version>
      </properties>
    </profile>
  </profiles>
</project>
